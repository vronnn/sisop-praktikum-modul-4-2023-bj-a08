# sisop-praktikum-modul-4-2023-BJ-A08

Kelompok A08 :
| Nama | NRP |
| ---------------------- | ---------- |
| Robby Ulung Pambudi | 5025211042 |
| Mohammad Zhafran Dzaky | 5025211142 |
| Ihsan Widagdo | 5025211231 |

## Soal 1

Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran.
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.

### Point A

#### Deskripsi

Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

kaggle datasets download -d bryanb/fifa-player-stats-database

Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut. Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya.

#### Penyelesaian

Pertama kita buat terlebih dahulu file storage.c lalu untuk mendownload kita lakukan perintah berikut :

```c
const char* kaggleCommand = "kaggle datasets download -d bryanb/fifa-player-stats-database";

    system(kaggleCommand);
```

Kemudian untuk unzip hasil downloadan tersebut, kita lakukan perintah berikut :

```c
char *args[] = {"unzip", "-q", sourceDir, "-d", ".", NULL};
    execv("/usr/bin/unzip", args);
```

Agar, file dapat didownload kita perlu mengkonfigurasi terlebih dahulu file kaggle.json yang didapat dari kaggle.com dan perlu kita lakukan perintah `pip install kaggle` terlebih dahulu pada terminal

#### Code

```c
void unzip(char *sourceDir){
  pid_t unzipID = fork();

  if (unzipID == 0){
    char *args[] = {"unzip", "-q", sourceDir, "-d", ".", NULL};
    execv("/usr/bin/unzip", args);
  }

  waitpid(unzipID, NULL, 0);
  remove(sourceDir);
}

void download(){
    const char* kaggleCommand = "kaggle datasets download -d bryanb/fifa-player-stats-database";

    system(kaggleCommand);
}
```

#### Penjelasan

1. `char *args[] = {"unzip", "-q", sourceDir, "-d", ".", NULL}` : Untuk mengunzip file hasil download lalu meremove file .zip yang telah diunzip
2. `system(kaggleCommand)` : Untuk menjalankan perintah `kaggle datasets download -d bryanb/fifa-player-stats-database` menggunakan fungsi system

### Point B

#### Deskripsi

Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.

#### Penyelesaian

Soal tersebut dapat diselesaikan menggunakan perintah AWK, kita dapat menjalankan perintah sebagai berikut untuk menampilkan nama, club, usia, potential, nationality, serta photourl dari masing masing pemain yang telah difilter

```c
system("awk -F\",\" 'NR > 1 && $3 < 25 && $8 > 85 && $9 != \"Manchester City\" { printf \"Name: %s\\n\", $2; printf \"Club: %s\\n\", $9; printf \"Age: %s\\n\", $3; printf \"Potential: %s\\n\", $8; printf \"Nationality: %s\\n\", $5; printf \"Photo URL: %s\\n\", $4; printf \"---------------\\n\" }' FIFA23_official_data.csv");
```

#### Code

```c
system("awk -F\",\" 'NR > 1 && $3 < 25 && $8 > 85 && $9 != \"Manchester City\" { printf \"Name: %s\\n\", $2; printf \"Club: %s\\n\", $9; printf \"Age: %s\\n\", $3; printf \"Potential: %s\\n\", $8; printf \"Nationality: %s\\n\", $5; printf \"Photo URL: %s\\n\", $4; printf \"---------------\\n\" }' FIFA23_official_data.csv");
```

#### Penjelasan

1. `awk` : Untuk menjalankan program awk
2. `-F\",\"` : Untuk mengatur pemisah (field separator) dalam data menjadi koma (",")
3. `'NR > 1 && $3 < 25 && $8 > 85 && $9 != \"Manchester City\"` : Merupakan kondisi filter yang digunakan untuk memilih baris-baris data yang memenuhi kriteria usia di bawah 25 dan potential lebih dari 85 serta tidak berasal dari club manchester city

### Point C

#### Deskripsi

Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.
Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.

#### Penyelesaian

Kita buat file baru dengan nama Dockerfile lalu kita gunakan base image sesuai sistem operasi yang kita gunakan, kemudian kita setup environmentnya dengan sesuai agar dapat menjalankan program storage.c, untuk setup pertama kita tentukan terlebih dahulu working directorynya menggunakan perintah berikut `WORKDIR /app` kemudian kita copy file kaggle.json serta storage.c ke dalam working directory kita pada docker menggunakan perintah berikut `COPY storage.c /app/` `COPY kaggle.json /root/.kaggle/kaggle.json` lalu kita lakukan perintah `RUN apt-get update && apt-get install -y gcc gawk python3 python3-pip unzip` untuk menginstall gcc, awk, python, dan unzip. Setelah itu kita jalankan perintah `RUN pip3 install kaggle` untuk menginstall kaggle pada docker dan kita compile program c kita dengan menjalankan perintah berikut `RUN gcc storage.c -o storage`, kemudian kita run program storage yang telah dicompile menggunakan perintah `CMD ["./storage"]`. Lalu setelah itu kita build dockerfile tersebut dengan perintah `sudo docker build -t storage-app .`, kemudian untuk menjalankan docker image yang telah kita build, kita ketikkan perintah berikut `sudo docker run storage-app`

#### Code

```Dockerfile
FROM kalilinux/kali-rolling

WORKDIR /app

COPY storage.c /app/
COPY kaggle.json /root/.kaggle/kaggle.json

RUN apt-get update && apt-get install -y gcc gawk python3 python3-pip unzip

RUN pip3 install kaggle

RUN gcc storage.c -o storage


CMD ["./storage"]

```

#### Penjelasan

1. `COPY storage.c /app/` : Untuk mengcopy storage.c ke working directory
2. `RUN apt-get update && apt-get install -y gcc gawk python3 python3-pip unzip` : Untuk menjalankan perintah `apt-get update` serta `apt-get install -y gcc gawk python3 python3-pip unzip`
3. `CMD ["./storage"]` : Untuk menjalankan perintah ./storage pada terminal

### Point D

#### Deskripsi

Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?
Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.

#### Penyelesaian

Kita perlu membuat akun dockerhub terlebih dahulu, lalu kita jalankan perintah `sudo docker login` lalu kita masukkan username dan password sesuai dengan akun yang telah kita buat, setelah itu kita lakukan perintah `sudo docker push dagdo/storage-app:latest` dimana dagdo03 merupakan username akun dockerhub saya, storage-app merupakan nama docker image yang telah saya build dan latest merupakan nama tag yang telah saya berikan ketika membuild docker image. Setelah itu file docker dapat dilihat pada link berikut `https://hub.docker.com/r/dagdo03/storage-app`

### Point E

#### Deskripsi

Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.

#### Penyelesaian

Pertama dibuat file baru dengan nama `docker-compose.yml`. Lalu kita setting agar docker dapat menjalankan instance sebanyak 5 sekaligus dengan susunan sebagai berikut

```yml
barcelona:
  image: dagdo03/storage-app
  build:
    context: ./Barcelona
    dockerfile: Dockerfile
  deploy:
    replicas: 5
  volumes:
    - .:/app/barcelona
```

dimana kita gunakan docker compose versi 3, lalu kita buat service dengan nama barcelona, napoli, dan storage-app dimana kita gunakan Dockerfile untuk membangun docker image pada servis yang kita buat lalu kita lakukan mounting volume dari host ke dalam kontainer untuk service tersebut. Lalu setelah itu kita buat direktori baru dengan nama Barcelona dan Napoli, lalu pada terminal kita lakukan perintah cd ke salah satu direktori tersebut. Setelah itu kita jalankan perintah `sudo docker-compose up` pada terminal untuk menjalankan docker-compose yang telah kita buat

#### Code

```yml
version: '3'

services:
  barcelona:
    image: dagdo03/storage-app
    build:
      context: ./Barcelona
      dockerfile: Dockerfile
    deploy:
      replicas: 5
    volumes:
      - .:/app/barcelona
  napoli:
    image: dagdo03/storage-app
    build:
      context: ./Napoli
      dockerfile: Dockerfile
    deploy:
      replicas: 5
    volumes:
      - .:/app/napoli
  storage-app:
    image: dagdo03/storage-app
    build:
      context: .
      dockerfile: Dockerfile
    deploy:
      replicas: 5
    volumes:
      - .:/app/main
```

#### Penjelasan

1. `version: '3'` : Untuk menggunakan docker compose versi 3
2. `services:` : Untuk menciptakan servis baru sesuai yang kita butuhkan
3. `instance1:` : Menciptakan servis baru dengan nama instance1
4. `context: .` : Untuk menggunakan direktori saat ini untuk membangun servis
5. `dockerfile: Dockerfile` : Menggunakan file Dockerfile untuk membangun docker image untuk servis yang kita bikin
6. `volumes: - ./app/barcelona` : Untuk melakukan mounting volume dari host ke dalam kontainer untuk service yang terkait

#### Output

![output_soal_1](/uploads/3afe4285c2409f6815d900dc0913e9c9/Screenshot_2023-06-03_13_36_11.png)

## Soal 2

Nana adalah peri kucing yang terampil dalam menggunakan sihir, dia bisa membuat orang lain berubah menjadi Molina. Suatu hari, dia bosan menggunakan sihir dan mencoba untuk magang di germa.dev sebagai programmer.

Agar dapat diterima sebagai karyawan magang dia diberi sebuah file .zip yang berisi folder dan file dari Germa. Kemudian, Nana harus membuat sistem manajemen folder dengan ketentuan sebagai berikut:

- Apabila terdapat file yang mengandung kata restricted, maka file tersebut tidak dapat di-rename ataupun dihapus.
- Apabila terdapat folder yang mengandung kata restricted, maka folder tersebut, folder yang ada di dalamny

**Contoh**:
/jalan/keputih/perintis/iv/`sangatrestrictedloh`/gang/kane.txt

Karena latar belakang Nana adalah seorang penyihir, dia ingin membuat satu kata sihir yaitu bypass yang dapat menghilangkan kedua aturan tadi. Kata sihir tersebut akan berlaku untuk folder dan file yang ada di dalamnya.

**Case invalid untuk bypass:**

- /jalan/keputih/perintis/iv/tidakrestrictedlohini/gang/kane.txt
- /jalan/keputih/perintis/iv/sangatrestrictedloh/gangIV/kane.txt
- /jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanelagiramai.txt

**Case valid untuk bypass:**

- /jalan/keputih/perintis/iv/tadirestrictedtapibypasskok/gang/kane.txt
- /jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kane.txt
- /jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kaneudahtutup.txt
- /jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanepakaibypass.txt

Setelah Nana paham aturan tersebut, bantu Nana untuk membuat sebuah FUSE yang bernama germa.c, yang mana dapat melakukan make folder, rename file dan folder, serta delete file dan folder. Untuk mengujinya, maka lakukan hal-hal berikut:

### Point A

a. Buatlah folder productMagang pada folder /src_data/germa/products/restricted_list/. Kemudian, buatlah folder projectMagang pada /src_data/germa/projects/restricted_list/. Akan tetapi, hal tersebut akan gagal.

#### Solusi

Untuk menyelesaikan soal tersebut pertama tama kita harus membuat sebuah aplikasi fuse yang bisa membaca dan mendapatkan isi dari sebuah directory. pada library fuse sendiri untuk membuat fungsi tersebut maka harus mendeklarasikan sebuah stuct sebagai berikut :

```c
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
}
```

**Penjelasan**

1. `getattr` : untuk mendapatkan atribut dari sebuah file atau directory yang ada di dalam sebuah directory
2. `readdir` : untuk membaca isi dari sebuah directory
3. `read` : untuk membaca isi dari sebuah file

Selanjutnya untuk fungsi pada xmp_getattr, xmp_readdir, dan xmp_read adalah sebagai berikut :

```c
static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}
```

_Penjelasan_

1. `sprintf(fpath,"%s%s",dirpath,path);` : untuk menyimpan path dari sebuah file atau directory yang akan diakses
2. `res = lstat(fpath, stbuf);` : untuk mendapatkan atribut dari sebuah file atau directory yang ada di dalam sebuah directory
3. `if (res == -1) return -errno;` : untuk mengecek apakah file atau directory yang akan diakses ada atau tidak jika tidak ada maka akan mengembalikan nilai -errno
4. `return 0;` : jika file atau directory yang akan diakses ada maka akan mengembalikan nilai 0

```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}
```

**Penjelasan**

1. `if(strcmp(path,"/") == 0)` : untuk mengecek apakah path yang akan diakses adalah root atau bukan
2. `sprintf(fpath, "%s%s",dirpath,path);` : untuk menyimpan path dari sebuah file atau directory yang akan diakses
3. `dp = opendir(fpath);` : untuk membuka directory yang akan diakses
4. `if (dp == NULL) return -errno;` : untuk mengecek apakah directory yang akan diakses ada atau tidak jika tidak ada maka akan mengembalikan nilai -errno
5. `while ((de = readdir(dp)) != NULL)` : untuk membaca isi dari sebuah directory
6. `struct stat st;` : untuk menyimpan atribut dari sebuah file atau directory yang ada di dalam sebuah directory
7. `memset(&st, 0, sizeof(st));` : untuk mengosongkan isi dari struct stat st
8. `st.st_ino = de->d_ino;` : untuk menyimpan inode dari sebuah file atau directory yang ada di dalam sebuah directory
9. `st.st_mode = de->d_type << 12;` : untuk menyimpan mode dari sebuah file atau directory yang ada di dalam sebuah directory
10. `res = (filler(buf, de->d_name, &st, 0));` : untuk mengisi isi dari sebuah directory
11. `if(res!=0) break;` : untuk mengecek apakah isi dari sebuah directory sudah terisi semua atau belum jika sudah maka akan mengembalikan nilai 0
12. `closedir(dp);` : untuk menutup directory yang telah dibuka
13. `return 0;` : untuk mengembalikan nilai 0

```c
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}
```

**Penjelasan**

1. `if(strcmp(path,"/") == 0)` : untuk mengecek apakah path yang akan diakses adalah root atau bukan
2. `sprintf(fpath, "%s%s",dirpath,path);` : untuk menyimpan path dari sebuah file atau directory yang akan diakses
3. `fd = open(fpath, O_RDONLY);` : untuk membuka file yang akan diakses
4. `if (fd == -1) return -errno;` : untuk mengecek apakah file yang akan diakses ada atau tidak jika tidak ada maka akan mengembalikan nilai -errno
5. `res = pread(fd, buf, size, offset);` : untuk membaca isi dari sebuah file yang akan diakses
6. `if (res == -1) res = -errno;` : untuk mengecek apakah isi dari sebuah file yang akan diakses ada atau tidak jika tidak ada maka akan mengembalikan nilai -errno
7. `close(fd);` : untuk menutup file yang telah dibuka
8. `return res;` : untuk mengembalikan nilai res
9. `return 0;` : untuk mengembalikan nilai 0
10. `return -errno;` : untuk mengembalikan nilai -errno

Selanjutnya untuk menyelesaikan point A diamana ketika membuat folder akan gagal maka kita membutuhkan satu buah fungsi dari fuse yaitu `mkdir`. Tujuan dari fungsi ini adalah untuk membuat sebuah directory yang didalamnya akan membuat sebuah aturan diamana ketika terdapat kata `restricted` maka program akan gagal membuat directory tersebut. Berikut adalah fungsi dari `mkdir` :

```c
static int xmp_mkdir(const char *path, mode_t mode)
{
    int res;
    char fpath[10000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);


    char *desc = malloc(9000);
    if (strstr(fpath, "restricted") != NULL)
    {
        sprintf(desc, "Create directory %s", fpath);
        logged("FAILED", "MKDIR", desc);
        return -1;
    }

    res = mkdir(fpath, mode);

    sprintf(desc, "Create directory %s", fpath);
    logged("SUCCESS", "MKDIR", desc);

    if (res == -1) return -errno;
    return 0;

}
```

**Penjelasan**

1. `if (strcmp(path, "/") == 0)` : untuk mengecek apakah path yang akan diakses adalah root atau bukan
2. `sprintf(fpath, "%s%s", dirpath, path);` : untuk menyimpan path dari sebuah file atau directory yang akan diakses
3. `char *desc = malloc(9000);` : untuk menyimpan deskripsi dari sebuah log
4. `if (strstr(fpath, "restricted") != NULL)` : untuk mengecek apakah path yang akan diakses mengandung kata `restricted` atau tidak
5. `sprintf(desc, "Create directory %s", fpath);` : untuk menyimpan deskripsi dari sebuah log
6. `logged("FAILED", "MKDIR", desc);` : untuk membuat log dengan tipe `FAILED` dan deskripsi yang telah disimpan
7. `res = mkdir(fpath, mode);` : untuk membuat directory yang akan diakses
8. `sprintf(desc, "Create directory %s", fpath);` : untuk menyimpan deskripsi dari sebuah log
9. `logged("SUCCESS", "MKDIR", desc);` : untuk membuat log dengan tipe `SUCCESS` dan deskripsi yang telah disimpan
10. `if (res == -1) return -errno;` : untuk mengecek apakah directory yang akan diakses ada atau tidak jika tidak ada maka akan mengembalikan nilai -errno
11. `return 0;` : untuk mengembalikan nilai 0

### Point B

Ubahlah nama folder dari restricted_list pada folder /src_data/germa/projects/restricted_list/ menjadi /src_data/germa/projects/bypass_list/. Kemudian, buat folder projectMagang di dalamnya.

#### Solusi

Untuk menyelesaikan point B ini kita membutuhkan satu buah fungsi dari fuse yaitu `rename`. Tujuan dari fungsi ini adalah untuk mengubah nama sebuah file atau directory. Berikut adalah fungsi dari `rename` :

```c
static int xmp_rename(const char *from, const char *to)
{
    int res;
    char ffrom[1000];
    char fto[1000];


    if (strcmp(from, "/") == 0)
    {
        from = dirpath;
        sprintf(ffrom, "%s", from);
    }
    else sprintf(ffrom, "%s%s", dirpath, from);

    if (strcmp(to, "/") == 0)
    {
        to = dirpath;
        sprintf(fto, "%s", to);
    }
    else sprintf(fto, "%s%s", dirpath, to);

    int flag = 0;

    if (strstr(ffrom, "bypass") != NULL || strstr(ffrom, "sihir") != NULL)
    {
        flag =  1;
    }

    if (strstr(ffrom, "restricted") != NULL){
        flag = 0;
    }

    if (strstr(fto, "bypass") != NULL || strstr(fto, "sihir") != NULL)
    {
        flag =  1;
    }
    if (strstr(ffrom, "bypass") == NULL && strstr(ffrom, "sihir") == NULL && strstr(ffrom, "restricted") == NULL){
        flag = 1;
    }

    char *desc = malloc(9000);
    if (flag == 0){
        sprintf(desc, "Rename from %s to %s", ffrom, fto);
        logged("FAILED", "RENAME", desc);
        return -1;
    }

    sprintf(desc, "Rename from %s to %s", ffrom, fto);
    res = rename(ffrom, fto);

    if (res == -1) return -errno;
    logged("SUCCESS", "RENAME", desc);

    return 0;
}
```

**Penjelasan**

- `if (strcmp(from, "/") == 0)` : untuk mengecek apakah path yang akan diakses adalah root atau bukan
- `sprintf(ffrom, "%s%s", dirpath, from);` : untuk menyimpan path dari sebuah file atau directory yang akan diakses
- `if (strcmp(to, "/") == 0)` : untuk mengecek apakah path yang akan diakses adalah root atau bukan
- `sprintf(fto, "%s%s", dirpath, to);` : untuk menyimpan path dari sebuah file atau directory yang akan diakses
- `int flag = 0;` : untuk menyimpan nilai 0
- `if (strstr(ffrom, "bypass") != NULL || strstr(ffrom, "sihir") != NULL)` : untuk mengecek apakah path yang akan diakses mengandung kata `bypass` atau `sihir` atau tidak
- `flag =  1;` : untuk menyimpan nilai 1
- `if (strstr(ffrom, "restricted") != NULL)` : untuk mengecek apakah path yang akan diakses mengandung kata `restricted` atau tidak
- `flag = 0;` : untuk menyimpan nilai 0
- `char *desc = malloc(9000);` : untuk menyimpan deskripsi dari sebuah log
- `if (flag == 0)` : untuk mengecek apakah nilai dari flag adalah 0 atau tidak
- `sprintf(desc, "Rename from %s to %s", ffrom, fto);` : untuk menyimpan deskripsi dari sebuah log
- `logged("FAILED", "RENAME", desc);` : untuk membuat log dengan tipe `FAILED` dan deskripsi yang telah disimpan
- `return -1;` : untuk mengembalikan nilai -1
- `sprintf(desc, "Rename from %s to %s", ffrom, fto);` : untuk menyimpan deskripsi dari sebuah log
- `res = rename(ffrom, fto);` : untuk mengubah nama sebuah file atau directory
- `if (res == -1) return -errno;` : untuk mengecek apakah directory yang akan diakses ada atau tidak jika tidak ada maka akan mengembalikan nilai -errno
- `logged("SUCCESS", "RENAME", desc);` : untuk membuat log dengan tipe `SUCCESS` dan deskripsi yang telah disimpan
- `return 0;` : untuk mengembalikan nilai 0

### Point C

Karena folder projects menjadi dapat diakses, ubahlah folder filePenting di dalam folder projects menjadi restrictedFilePenting agar secure kembali. Coba keamanannya dengan mengubah nama file yang ada di dalamnya.

#### Solusi

Untuk menyelesaikan point C ini kita membutuhkan satu buah fungsi dari fuse yaitu `rename`. Tujuan dari fungsi ini adalah untuk mengubah nama sebuah file atau directory. Kode seperti ini sudah ada di point B.

### Point D

Semua kegiatan yang kalian lakukan harus tercatat dalam sebuah file logmucatatsini.txt di luar file .zip yang diberikan kepada Nana, agar pekerjaan yang kamu lakukan dapat dilaporkan kepada Germa.

#### Solusi

Untuk menyelesaikan point D ini kita membutuhkan satu buah fungsi `logged` yang telah dibuat di point A dan B. Tujuan dari fungsi ini adalah untuk membuat log. Berikut adalah fungsi dari `logged` :

```c
void logged(char *level, char *cmd, char *desc)
{
    FILE *fp;

    fp = fopen("/home/robby/sisop/sisop-praktikum-modul-4-2023-bj-a08/soal2/logmucatatsini.txt", "a+");

    if (fp == NULL){
        printf("[Error] : [Gagal dalam membuka file]");
        exit(1);
    }
    char *user = getUserName();
    char *description = malloc(10000 * sizeof(char));
    sprintf(description, "%s-%s", user, desc);

    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    fprintf(fp, "[%s]::%02d/%02d/%04d-%02d:%02d:%02d::[%s]::[%s]\n", level, tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec, cmd, description);
    fclose(fp);
}
```

**Penjelasan**

- `fp = fopen("/home/robby/sisop/sisop-praktikum-modul-4-2023-bj-a08/soal2/logmucatatsini.txt", "a+");` : untuk membuka file `logmucatatsini.txt` dengan mode `a+`
- `if (fp == NULL)` : untuk mengecek apakah file `logmucatatsini.txt` ada atau tidak
- `char *user = getUserName();` : untuk menyimpan username dari user yang sedang login
- `char *description = malloc(10000 * sizeof(char));` : untuk menyimpan deskripsi dari sebuah log
- `sprintf(description, "%s-%s", user, desc);` : untuk menyimpan deskripsi dari sebuah log
- `time_t t = time(NULL);` : untuk menyimpan waktu sekarang
- `struct tm tm = *localtime(&t);` : untuk menyimpan waktu sekarang
- `fprintf(fp, "[%s]::%02d/%02d/%04d-%02d:%02d:%02d::[%s]::[%s]\n", level, tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec, cmd, description);` : untuk membuat log dengan format yang telah ditentukan
- `fclose(fp);` : untuk menutup file `logmucatatsini.txt`
- `exit(1);` : untuk menghentikan program

### Kendala

- Kesulitan dalam memahami soal
- Kesulitan memahami fuse

### Output
![Screen_Shot_2023-06-03_at_21.04.55](/uploads/775dd38da359570d6d128d537e214459/Screen_Shot_2023-06-03_at_21.04.55.png)

## Soal 3

Dhafin adalah seorang yang sangat pemalu, tetapi saat ini dia sedang mengagumi seseorang yang dia tidak ingin seorangpun tahu. Maka dari itu, dia melakukan berbagai hal sebagai berikut.

### Point A

#### Deskripsi

Dhafin ingin membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah /etc yang bernama secretadmirer.c.

#### Penyelesaian

Kita dapat membuat program FUSE sederhana yang hanya menggunakan 3 atribut operasi, yakni `getattr`, `readdir`, dan `read`. yang mana masing - masing atribut operasi digunakan untuk mendapatkan file sistem yang akan di mount, memuat semua directory yang ada di dalamnya, dan membaca isi konten dari masing - masing file. Berikut ini adalah contoh program yang dapat diambil dari modul,

```c
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>

static  const  char *dirpath = "/home/[user]/Documents";

static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}



static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}



static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}



static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
};



int  main(int  argc, char *argv[])
{
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```

### Point B

#### Deskripsi

Lalu, untuk menutupi rahasianya, Dhafin ingin semua direktori dan file yang berada pada direktori yang diawali dengan huruf L, U, T, dan H akan ter-encode dengan Base64. Encode berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat).

#### Penyelesaian

Untuk menyelesaikan point ini, maka pada saat kita akan memuat semua directory dan file yang ada, kita perlu cek terlebih dahulu apakah namanya berawalan huruf L, U, T, dan H. Hal ini dapat kita lakukan dengan membuat fungsi pengecekan secara terpisah agar kode terlihat lebih rapi, sebagai berikut,

```c
bool is_encoded(const char *name) {
    char first_char = name[0];
    if (first_char == 'l' || first_char == 'L' ||
        first_char == 'u' || first_char == 'U' ||
        first_char == 't' || first_char == 'T' ||
        first_char == 'h' || first_char == 'H') {
        return true;
    }
    return false;
}
```

Fungsi ini bertipe boolean yang mengambil argumen berupa nama file dan akan mengembalikan true jika nama file tersebut berawalan L, U, T, H dan mengembalikan false jika tidak. Barulah kita panggil fungsi ini dalam operasi FUSE yang memuat semua directory dan file yang ada, yakni `readdir` seperti berikut,

```c
if(is_encoded(name)) {
    encodeBase64File(cur_path);
}
```

Setelah itu, barulah kita buat fungsi `encodeBase64File` untuk meng-encode file pada path yang diberikan melalui argumen sebagai berikut,

```c
static const char base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void encodeBase64File(const char *filename) {
    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        printf("Failed to open file: %s\n", filename);
        return;
    }

    // Get file size
    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    // Allocate memory for file content
    uint8_t *file_content = (uint8_t *)malloc(file_size);
    if (file_content == NULL) {
        printf("Memory allocation failed\n");
        fclose(file);
        return;
    }

    // Read file content
    size_t bytes_read = fread(file_content, 1, file_size, file);
    fclose(file);

    if (bytes_read != file_size) {
        printf("Failed to read file: %s\n", filename);
        free(file_content);
        return;
    }

    // Encode file content as Base64
    uint8_t input[3], output[4];
    size_t input_len = 0;
    size_t output_len = 0;
    size_t i = 0;

    file = fopen(filename, "wb");
    if (file == NULL) {
        printf("Failed to open file for writing: %s\n", filename);
        free(file_content);
        return;
    }

    while (i < file_size) {
        input[input_len++] = file_content[i++];
        if (input_len == 3) {
            output[0] = base64_table[input[0] >> 2];
            output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
            output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
            output[3] = base64_table[input[2] & 0x3F];

            fwrite(output, 1, 4, file);

            input_len = 0;
            output_len += 4;
        }
    }

    if (input_len > 0) {
        for (size_t j = input_len; j < 3; j++) {
            input[j] = 0;
        }

        output[0] = base64_table[input[0] >> 2];
        output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
        output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
        output[3] = base64_table[input[2] & 0x3F];

        fwrite(output, 1, input_len + 1, file);

        for (size_t j = input_len + 1; j < 4; j++) {
            fputc('=', file);
        }

        output_len += 4;
    }

    fclose(file);
    free(file_content);

    printf("File encoded successfully. file: %s\n", filename);
}
```

Berikut ini adalah penjelasan fungsi encode di atas,

- Pertama, fungsi ini mengambil argumen berupa nama file yang berupa path ke file yang ingin diencode.
- File dibuka dalam mode biner untuk dibaca menggunakan fungsi fopen. Jika file tidak dapat dibuka, pesan kesalahan dicetak dan dikembalikan.
- Ukuran file ditentukan dengan menggunakan fungsi fseek dan ftell. Pertama, penunjuk file dipindahkan ke akhir file (SEEK_END), kemudian posisi saat ini dari penunjuk file diambil, yang memberikan ukuran file. Setelah itu, penunjuk file dikembalikan ke awal file (SEEK_SET).
- Memori dialokasikan untuk menyimpan konten file. Ukuran memori yang dialokasikan sama dengan ukuran file. Jika alokasi memori gagal, pesan kesalahan dicetak, file ditutup, dan dikembalikan.
- Konten file dibaca sepenuhnya ke dalam memori yang dialokasikan menggunakan fungsi fread. Jumlah byte yang dibaca dikembalikan oleh fungsi tersebut. Jika jumlah byte yang dibaca tidak sama dengan ukuran file, berarti terjadi kesalahan saat membaca file. Dalam kasus tersebut, pesan kesalahan dicetak, memori yang dialokasikan dibebaskan, file ditutup, dan dikembalikan.
- Variabel diinisialisasi dan file dibuka kembali, kali ini dalam mode biner untuk ditulis menggunakan fungsi fopen. Jika file tidak dapat dibuka, pesan kesalahan dicetak, memori yang dialokasikan dibebaskan, dan dikembalikan.
- Kemudian masuk ke dalam loop yang mengiterasi konten file. Di dalam loop, konten file dibaca byte per byte dan disimpan dalam array input. Juga diperiksa apakah array input telah mengumpulkan tiga byte.
  Jika tiga byte telah terkumpul, tiga byte tersebut dienkripsi menjadi empat karakter Base64 dan ditulis ke file menggunakan fwrite. Variabel output_len diperbarui untuk melacak jumlah karakter yang telah ditulis.
- Setelah loop, dicek apakah ada byte yang tersisa dalam array input. Jika ada, byte yang tersisa dipad dengan nol. Byte yang tersisa dienkripsi menjadi karakter Base64, ditulis ke file, dan output_len diperbarui sesuai.
- Karakter padding (=) ditambahkan ke file jika diperlukan. Jumlah karakter padding tergantung pada jumlah byte yang tersisa dalam array input.
- File ditutup dan memori yang dialokasikan dibebaskan.
  Akhirnya, pesan keberhasilan dicetak yang menunjukkan bahwa file telah berhasil dienkripsi.

### Point C

#### Deskripsi

Untuk membedakan antara file dan direktori, Dhafin ingin semua format penamaan file menggunakan lowercase sedangkan untuk direktori semua harus uppercase. Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.

#### Penyelesaian

Hal ini sama dengan sebelumnya, kita hanya perlu mengecek apakah yang sedang dimuat adalah sebuah direktori atau sebuah file, jika file maka ubah namanya menjadi huruf kecil dan jika directory, maka ubah namanya menjadi huruf besar. Namun sebelum melakukan hal tersebut, kita dapat mempersempit ketidakefisienan dalam modifikasi ini, yakni dengan melakukan pengecekan jumlah huruf pada nama direktori atau file yang akan dimuat, jika kurang dari sama dengan 4, maka langsung encode saja namanya menjadi kode biner masing - masing karakternya, jika tidak baru ubah namanya menjadi huruf besar atau kecil sesuai dengan jenisnya. Berikut ini adalah fungsi encode binary, huruf besar, dan huruf kecil secara berurutan.

**Encode Binary**

```c
void convert_to_binary(char *name)
{
    int len = strlen(name);
    char *temp = (char *)malloc((8 * len + len) * sizeof(char)); // Allocate memory for modified string

    int index = 0; // Index for the modified string

    for (int i = 0; i < len; i++)
    {
        char ch = name[i];

        // Convert character to binary string
        for (int j = 7; j >= 0; j--)
        {
            temp[index++] = (ch & 1) + '0'; // Convert bit to character '0' or '1'
            ch >>= 1;                       // Shift right by 1 bit
        }

        if (i != len - 1)
        {
            temp[index++] = ' '; // Add space separator between binary codes
        }
    }

    temp[index] = '\0'; // Null-terminate the modified string

    strcpy(name, temp); // Update the original string with the modified string
    free(temp);         // Free the allocated memory
}
```

- Fungsi ini dimulai dengan menghitung panjang string input name menggunakan fungsi strlen dan menyimpannya dalam variabel len.
- Kemudian memori dialokasikan untuk string yang dimodifikasi bernama temp. String yang dimodifikasi akan berisi representasi biner karakter-karakter dalam string asli, dipisahkan oleh spasi. Jumlah memori yang dialokasikan adalah (8 _ len + len) _ sizeof(char). Faktor 8 \* len mengakomodasi representasi biner setiap karakter, dan faktor + len mengakomodasi spasi tambahan antara kode biner.
- Variabel indeks diinisialisasi untuk melacak posisi dalam string yang dimodifikasi. Indeks ini akan digunakan untuk mengisi string temp.
- Kemudian masuk ke dalam loop yang mengiterasi setiap karakter dalam string input name.
- Di dalam loop, setiap karakter ch diubah menjadi representasi binernya. Hal ini dilakukan dengan mengiterasi bit-bit karakter dari bit yang paling signifikan (bit 7) hingga bit yang paling tidak signifikan (bit 0).
- Untuk setiap bit, bit paling tidak signifikan dari ch diekstrak menggunakan operasi bitwise AND (ch & 1). Hasil dari operasi bitwise AND ditambahkan dengan karakter '0' untuk mengubah bit menjadi karakter ('0' atau '1'). Kemudian, karakter ini disimpan dalam string temp pada posisi indeks saat ini dan indeks ditingkatkan.
- Setelah mengambil bit paling tidak signifikan, bit-bit dari ch digeser ke kanan sejauh 1 posisi (ch >>= 1) untuk memproses bit berikutnya.
- Setelah semua bit dari suatu karakter dikonversi dan disimpan dalam string temp, diperiksa apakah bukan karakter terakhir dalam string input (i != len - 1).
- Jika bukan karakter terakhir, ditambahkan karakter spasi ke string temp untuk memisahkan kode biner dari karakter-karakter yang berbeda.
- Setelah memproses semua karakter dalam string input, string temp diberi terminasi nol dengan mengatur temp[indeks] menjadi '\0'.
- salin string yang dimodifikasi temp kembali ke string asli name menggunakan strcpy. Hal ini memperbarui string asli dengan representasi binernya.
- Yang terakhir adalah bebaskan memori yang dialokasikan untuk string temp menggunakan fungsi free.

**Ubah menjadi huruf besar**

```c
void modify_directoryname(char *name)
{
    for (int i = 0; name[i]; i++)
    {
        name[i] = toupper(name[i]);
    }
}
```

**Ubah menjadi huruf besar**

```c
void modify_filename(char *name)
{
    for (int i = 0; name[i]; i++)
    {
        name[i] = tolower(name[i]);
    }
}
```

- Kedua fungsi di atas ini sama - sama mengambil argumen berupa nama file.
- Kemudian dibuat sebuah loop untuk mengiterasi tiap - tiap karakter dalam nama file tersebut dan konversikan menjadi huruf besar (menggunakan fungsi `toupper`) atau huruf kecil (menggunakan fungsi `tolower`)

### Catatan

Ingat bahwa poin B dan C ini dilakukan secara rekursif agar dapat menjangkau semua file dan direktori yang ada. Oleh karena itu, perlu sebuah variabel yang menyimpan path yang sekarang sedang ditelusuri. Dalam kasus ini, kami menggunakan variabel array of char bernama `cur_path`. Variabel ini akan menyimpan path yang sedang ditelusuri saat rekursi. Namun, karena pathnya akan terus berubah, maka kita perlu sebuah fungsi yang juga berfungsi untuk mengubah nama path tersebut, yakni fungsi `change_path`. Fungsi ini akan mengubah path yang lama dengan yang baru sesuai dengan posisi traverse rekursi sekarang sehingga fungsi ini perlu untuk terus dipanggil saat memuat file system. Berikut ini adalah fungsi `change_path` yang kami gunakan,

```c
void changePath(char *str1, char *str2) {
    char *slash = strrchr(str1, '/');
    char temp[100];
    strcpy(temp, str1);
    if (slash != NULL) {
        ++slash; // Move past the slash
        strcpy(slash, str2);
        rename(temp, str1);
    }
}
```

- Fungsi ini dimulai dengan mendeklarasikan variabel pointer slash dan mengisinya dengan hasil pemanggilan strrchr(str1, '/'). Fungsi strrchr mencari kemunculan terakhir karakter '/' dalam string str1 dan mengembalikan pointer ke posisi tersebut dalam string. Pointer slash ini akan digunakan untuk menemukan nama file dalam jalur.

- kemudian deklarasikan array karakter temp dengan ukuran 100. Array ini akan digunakan untuk menyimpan salinan sementara dari jalur asli.

- salin isi string str1 ke dalam array temp menggunakan strcpy. Ini dilakukan untuk mempertahankan jalur asli karena akan dimodifikasi.

- periksa apakah pointer slash tidak NULL, yang berarti bahwa karakter slash ditemukan dalam string str1. Jika karakter slash ditemukan, menunjukkan bahwa ada nama file dalam jalur yang dapat diganti.

- Jika karakter slash ditemukan, menambahkan nilai pointer slash sebanyak satu untuk melewati karakter slash. Hal ini memposisikan pointer slash pada awal nama file dalam jalur.

- kemudian salin isi string str2 ke posisi pointer slash dalam string str1. Ini menggantikan nama file dalam jalur asli dengan nama file baru yang diberikan dalam str2.

- dan yang terakhir, panggil fungsi rename, dengan argumen temp array (jalur asli) dan str1 (jalur yang telah dimodifikasi) untuk mengganti nama file atau direktori. Dalam kasus ini, digunakan untuk mengganti nama file dengan nama file baru dalam jalur yang telah dimodifikasi.

### Point D

#### Deskripsi

Setiap ingin membuka file, maka harus memasukkan password terlebih dahulu (set password tidak ditentukan a.k.a terserah).

#### Penyelesaian

Untuk dapat melakukan hal ini, maka kita perlu menggunakan 1 atribut operasi dari FUSE lagi, yakni open. Di sini lah kita dapat membuat alur untuk harus mengisikan password yang benar saat akan membuka file. Berikut ini adalah potongan kodenya,

```c
static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    (void)fi;

    if (strcmp(path, "/") == 0) {
        path = dirpath;
    } else {
        char fpath[1000];
        sprintf(fpath, "%s%s", dirpath, path);
        if (access(fpath, F_OK) == 0) {
            char input_password[100];
            printf("Masukkan password: ");
            fflush(stdout);
            fgets(input_password, sizeof(input_password), stdin);

            size_t len = strlen(input_password);
            if (len > 0 && input_password[len - 1] == '\n')
                input_password[len - 1] = '\0';

            while (strcmp(input_password, password) != 0) {
                printf("Maaf, password salah! Silahkan coba kembali.\nMasukkan password: ");
                fflush(stdout);
                fgets(input_password, sizeof(input_password), stdin);

                len = strlen(input_password);
                if (len > 0 && input_password[len - 1] == '\n')
                    input_password[len - 1] = '\0';
            }
        }
    }

    return 0;
}
```

- Fungsi menerima path file yang akan dibuka dan objek struct fuse_file_info, tetapi parameter fi tidak digunakan dalam kode (oleh karena itu ada baris (void)fi;).

- periksa apakah path sama dengan "/", yang menunjukkan bahwa direktori root sedang diakses. Jika iya, maka path diperbarui menjadi nilai dari dirpath. Jika bukan direktori root ("/"), dilakukan langkah-langkah berikut:

- deklarasikan array karakter fpath dengan ukuran 1000. Array ini akan digunakan untuk menyimpan jalur lengkap ke file, termasuk dirpath dan jalur yang diberikan.

- kemudian buat path lengkapnya dengan menggunakan sprintf untuk menggabungkan dirpath dan path, dan menyimpannya dalam array fpath.

- Memeriksa apakah file ada di path yang lengkap yang telah dimuat dengan menggunakan fungsi access dengan flag F_OK. Fungsi access memeriksa aksesibilitas file berdasarkan izin yang ditentukan. Dalam kasus ini, flag F_OK memeriksa apakah file ada. Jika file ada, dilanjutkan dengan langkah-langkah berikutnya. Jika tidak, tidak dilakukan apa pun dan mengembalikan 0.

- kemudian minta pengguna memasukkan kata sandi dengan mencetak pesan "Masukkan password: " menggunakan printf.

- baca input password pengguna dari masukan standar menggunakan fgets dan menyimpannya dalam array input_password, yang memiliki ukuran 100.

- hapus karakter newline di akhir password masukan dengan memeriksa apakah karakter terakhir adalah '\n' dan menggantinya dengan '\0' jika ada.

- kemudian masuk ke dalam perulangan yang berulang kali memeriksa apakah password masukan cocok dengan password yang benar (password). Jika password tidak cocok, mencetak pesan kesalahan, meminta pengguna memasukkan password lagi, dan mengulang proses tersebut hingga password yang benar dimasukkan.

- Dalam perulangan tersebut, mencetak pesan kesalahan dan meminta pengguna memasukkan password lagi dengan mencetak pesan "Maaf, password salah! Silahkan coba kembali.\nMasukkan password: " menggunakan printf.

- baca lagi password masukan pengguna dari masukan standar menggunakan fgets dan menyimpannya dalam array input_password.

- hapus lagi karakter newline di akhir password masukan dengan memeriksa apakah karakter terakhir adalah '\n' dan menggantinya dengan '\0' jika ya.

- Setelah password yang benar dimasukkan, atau jika file tidak memerlukan pemeriksaan password (misalnya, jika file tidak ada), fungsi mengembalikan 0 untuk menunjukkan operasi pembukaan file yang berhasil.

**berikut ini adalah kode akhir dari program FUSE yang telah kita modifikasi**

### Code

```c
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <limits.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

static const char *dirpath = "/home/vron/sisop/praktikum-4/inifolderetc/sisop";
// static const char *mountpoint = "/home/vron/sisop/praktikum-4/tesdir4";
static const char *password = "bismillah";

static const char base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void encodeBase64File(const char *filename) {
    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        printf("Failed to open file: %s\n", filename);
        return;
    }

    // Get file size
    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    // Allocate memory for file content
    uint8_t *file_content = (uint8_t *)malloc(file_size);
    if (file_content == NULL) {
        printf("Memory allocation failed\n");
        fclose(file);
        return;
    }

    // Read file content
    size_t bytes_read = fread(file_content, 1, file_size, file);
    fclose(file);

    if (bytes_read != file_size) {
        printf("Failed to read file: %s\n", filename);
        free(file_content);
        return;
    }

    // Encode file content as Base64
    uint8_t input[3], output[4];
    size_t input_len = 0;
    size_t output_len = 0;
    size_t i = 0;

    file = fopen(filename, "wb");
    if (file == NULL) {
        printf("Failed to open file for writing: %s\n", filename);
        free(file_content);
        return;
    }

    while (i < file_size) {
        input[input_len++] = file_content[i++];
        if (input_len == 3) {
            output[0] = base64_table[input[0] >> 2];
            output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
            output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
            output[3] = base64_table[input[2] & 0x3F];

            fwrite(output, 1, 4, file);

            input_len = 0;
            output_len += 4;
        }
    }

    if (input_len > 0) {
        for (size_t j = input_len; j < 3; j++) {
            input[j] = 0;
        }

        output[0] = base64_table[input[0] >> 2];
        output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
        output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
        output[3] = base64_table[input[2] & 0x3F];

        fwrite(output, 1, input_len + 1, file);

        for (size_t j = input_len + 1; j < 4; j++) {
            fputc('=', file);
        }

        output_len += 4;
    }

    fclose(file);
    free(file_content);

    printf("File encoded successfully. file: %s\n", filename);
}

bool is_encoded(const char *name) {
    char first_char = name[0];
    if (first_char == 'l' || first_char == 'L' ||
        first_char == 'u' || first_char == 'U' ||
        first_char == 't' || first_char == 'T' ||
        first_char == 'h' || first_char == 'H') {
        return true;
    }
    return false;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}

void modify_filename(char *name)
{
    for (int i = 0; name[i]; i++)
    {
        name[i] = tolower(name[i]);
    }
}

void modify_directoryname(char *name)
{
    for (int i = 0; name[i]; i++)
    {
        name[i] = toupper(name[i]);
    }
}

void convert_to_binary(char *name)
{
    int len = strlen(name);
    char *temp = (char *)malloc((8 * len + len) * sizeof(char)); // Allocate memory for modified string

    int index = 0; // Index for the modified string

    for (int i = 0; i < len; i++)
    {
        char ch = name[i];

        // Convert character to binary string
        for (int j = 7; j >= 0; j--)
        {
            temp[index++] = (ch & 1) + '0'; // Convert bit to character '0' or '1'
            ch >>= 1;                       // Shift right by 1 bit
        }

        if (i != len - 1)
        {
            temp[index++] = ' '; // Add space separator between binary codes
        }
    }

    temp[index] = '\0'; // Null-terminate the modified string

    strcpy(name, temp); // Update the original string with the modified string
    free(temp);         // Free the allocated memory
}

void changePath(char *str1, char *str2) {
    char *slash = strrchr(str1, '/');
    char temp[100];
    strcpy(temp, str1);
    if (slash != NULL) {
        ++slash; // Move past the slash
        strcpy(slash, str2);
        rename(temp, str1);
    }
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        char name[100];
        strcpy(name, de->d_name);
        char cur_path[1500];
        sprintf(cur_path, "%s/%s", fpath, de->d_name);

        res = (filler(buf, name, &st, 0));

        if (res != 0)
            break;
        if(de->d_type == DT_REG) {
            if(is_encoded(name)) {
                encodeBase64File(cur_path);
            }
            else if(strlen(name) <= 4) {
                convert_to_binary(name);
            }
            else {
                modify_filename(name);
            }
            changePath(cur_path, name);
        }
        else {
            if(strlen(name) <= 4) {
                convert_to_binary(name);
            }
            else {
                modify_directoryname(name);
            }
            changePath(cur_path, name);
            xmp_readdir(cur_path, buf, filler, offset, fi);
        }
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);
    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    (void)fi;

    if (strcmp(path, "/") == 0) {
        path = dirpath;
    } else {
        char fpath[1000];
        sprintf(fpath, "%s%s", dirpath, path);
        if (access(fpath, F_OK) == 0) {
            char input_password[100];
            printf("Masukkan password: ");
            fflush(stdout);
            fgets(input_password, sizeof(input_password), stdin);

            size_t len = strlen(input_password);
            if (len > 0 && input_password[len - 1] == '\n')
                input_password[len - 1] = '\0';

            while (strcmp(input_password, password) != 0) {
                printf("Maaf, password salah! Silahkan coba kembali.\nMasukkan password: ");
                fflush(stdout);
                fgets(input_password, sizeof(input_password), stdin);

                len = strlen(input_password);
                if (len > 0 && input_password[len - 1] == '\n')
                    input_password[len - 1] = '\0';
            }
        }
    }

    return 0;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    // Modify the directory name to all uppercase
    char modified_name[256];
    strcpy(modified_name, path);
    for (int i = 0; modified_name[i] != '\0'; i++) {
        modified_name[i] = toupper(modified_name[i]);
    }

    // Create the directory with the modified name
    int res = mkdir(fpath, mode);
    if (res == -1) {
        return -errno;
    }

    return 0;
}

static int xmp_rename(const char *from, const char *to)
{
    char fpath_from[1000];
    char fpath_to[1000];
    sprintf(fpath_from, "%s%s", dirpath, from);
    sprintf(fpath_to, "%s%s", dirpath, to);

    // Modify the destination name to all uppercase
    char modified_name[256];
    strcpy(modified_name, to);
    for (int i = 0; modified_name[i] != '\0'; i++) {
        modified_name[i] = toupper(modified_name[i]);
    }

    // Rename the file/directory with the modified name
    int res = rename(fpath_from, fpath_to);
    if (res == -1) {
        return -errno;
    }

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .open = xmp_open,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
};

int main(int argc, char *argv[])
{
    umask(0);

    // Run the FUSE filesystem
    int fuse_stat = fuse_main(argc, argv, &xmp_oper, NULL);

    return fuse_stat;
}
```

### Point E

#### Deskripsi

Selanjutnya, Dhafin ingin melakukan mounting FUSE tersebut di dalam Docker Container menggunakan Docker Compose (gunakan base image Ubuntu Bionic ). Ketentuannya, terdapat dua buah container sebagai berikut.

- Container bernama Dhafin akan melakukan mount FUSE yang telah dimodifikasi tersebut.
- Container bernama Normal akan melakukan mount yang hanya menampilkan /etc yang normal tanpa ada modifikasi apapun.

#### Penyelesaian

Karena kita memerlukan 2 container, maka kita dapat menggunakan docker-compose untuk mengatur kedua container kita tersebut. Konfigurasi yang akan digunakan dapat menyesuaikan apa yang akan kita lakukan pada kedua container tersebut. Yang mana dalam kasus ini, kita akan melakukan bind mount folder asal yang telah kita mount di local ke dalam container. dimana container Dhafin akan berisi mem bind mount folder yang dimount menggunakan FUSE yang termodifikasi dan container normal akan mem bind mount folder etc kita yang dimount menggunakan FUSE normal, berikut ini adalah isi dari docker-compose yang sudah menyesuaikan konfigurasi untuk objektif yang ingin kita capai,

```yml
version: '3'

services:
  dhafin:
    image: ubuntu:bionic
    container_name: Dhafin
    command: tail -f /dev/null
    volumes:
      - /home/vron/sisop/praktikum-4/inifolderetc/sisop:/dhafin

  normal:
    image: ubuntu:bionic
    container_name: Normal
    command: tail -f /dev/null
    volumes:
      - /home/vron/sisop/praktikum-4/etc:/normal
```

- pertama kita tentukan versi docker-compose yang akan kita gunakan (dalam kasus ini kita gunakan versi 3)
- tentukan nama service yang akan dibangun, disini kita bedakan untuk service yang menghandle container Dhafin dan service yang menghandle container Normal
- kemudian kita akan menggunakan base image ubuntu bionic
- Beri nama untuk masing - masing container
- command: tail -f /dev/null adalah perintah untuk mengarahkan keluaran standar ke /dev/null, sehingga kontainer tetap berjalan tanpa melakukan tindakan apa pun. Anda dapat mengganti perintah ini sesuai kebutuhan aplikasi Anda.
- dan tentukan volume yang akan dipasangkan ke container dan akan dipasangkan dimana. Kedua hal ini dipisah dengan tanda `:`

Setelah semua persiapan sudah dilakukan, maka kita dapat mengeksekusi programnya. Pertama - tama, kumpulkan semua directori dan file yang kita butuhkan untuk problem ini dalam sebuah directory, termasuk docker-compose, inifolderetc, etc, program FUSE, dll. Kemudian mulai dengan compile dan run kedua program FUSE yang telah kita buat sebelumnya, satu yang telah termodifikasi dan satu yang belum. Untuk mengcompile program FUSE, kita dapat jalankan command ini di terminal,

```bash
gcc -Wall `pkg-config fuse --cflags` [file.c] -o [output] `pkg-config fuse --libs`
```

Lalu run kedua executable file yang telah dihasilkan dari compile sebelumnya dengan command dibawah ini untuk modified FUSE dan normal FUSE secara berurutan,

```bash
./[output] -f <dirtujuan>
```

```bash
./[output] <dirtujuan>
```

setelah semua file sudah terencode (dapat dilihat dari print sukses yang telah kita buat pada fungsi encode), barulah kita jalankan docker compose yang telah kita buat juga dengan jalankan command berikut ini di terminal,

```bash
sudo docker compose up -d
```

Lalu setelah itu, untuk mengecek apakah container sudah running atau belum, dapat kita jalan command berikut ini,

```bash
sudo docker ps
```

setelah sudah terkonfirmasi bahwa container sudah jalan, kita dapat cek apakah mount berhasil dilakukan atau tidak dengan masuk ke masing - masing container dan lihat pada folder yang telah kita tentukan pada docker-compose dengan command berikut ini,

```bash
sudo docker exec -it <nama_container> /bin/bash
```

Selesai, dapat dilihat bahwa sudah terdapat file system hasil FUSE di dalam container. berikut ini adalah bukti berhasil menjalankan hal - hal di atas,

![image](/uploads/b6f92c6806341464b0fbf530ae8f2fcc/image.png)
![image](/uploads/39dba549bcde1f20f14432858ecaab95/image.png)
![image](/uploads/f138f34eca02c897ad90c67f0e2fe9a1/image.png)

kemudian run

### Point F

#### Deskripsi

Kalian masih penasaran siapa yang dikagumi Dhafin? 🥰😍😘😚🤗😻💋💌💘💝💖💗 Coba eksekusi image fhinnn/sisop23 deh, siapa tahu membantu. 🙂

#### Penyelesaian

Untuk menyelesaikan poin ini, kita dapat menjalankan command berikut ini di terminal terlebih dahulu,

```bash
sudo docker run fhinnn/sisop23
```

![image](/uploads/31de30fb82b94524a1173d3bfc9f297c/image.png)

setelah membaca clue yang diberikan dan mencari tahu mengenai directory etc di linux, kami menemukan bahwa terdapat folder yang berbeda dan tidak normal yang berada di direktori inifolderetc pada soal, yakni pada direktori `kyaknysihflag`. Dan setelah membuka directory tersebut terdapat sebuah file txt yang isinya adalah kode biner seperti berikut ini,

![image](/uploads/eff09e626335c16db77c3c2fcfd6b8b1/image.png)
![image](/uploads/a46d03bcaf4bb6d0aebf84e8d1f38c7d/image.png)

dan setelah mengonversinya ke bentuk teks normal, didaptkan bahwa orang yang dikagumi mas Dhafin adalah,
![image](/uploads/48337d3178ef1e9f3908825ed6466559/image.png)
