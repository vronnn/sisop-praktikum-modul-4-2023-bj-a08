#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#include <pwd.h>
#include <regex.h>
#include <sys/stat.h>


void unzip(char *sourceDir){
  pid_t unzipID = fork();

  if (unzipID == 0){
    char *args[] = {"unzip", "-q", sourceDir, "-d", ".", NULL};
    execv("/usr/bin/unzip", args);
  }

  waitpid(unzipID, NULL, 0);
  remove(sourceDir);
}

void download(){
    const char* kaggleCommand = "kaggle datasets download -d bryanb/fifa-player-stats-database";

    system(kaggleCommand);
}
int main() {
    download();
    unzip("fifa-player-stats-database.zip");
    system("awk -F\",\" 'NR > 1 && $3 < 25 && $8 > 85 && $9 != \"Manchester City\" { printf \"Name: %s\\n\", $2; printf \"Club: %s\\n\", $9; printf \"Age: %s\\n\", $3; printf \"Potential: %s\\n\", $8; printf \"Nationality: %s\\n\", $5; printf \"Photo URL: %s\\n\", $4; printf \"---------------\\n\" }' FIFA23_official_data.csv");
    
    return 0;
}
