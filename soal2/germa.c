#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdlib.h>
#include <pwd.h>

static const char *dirpath = "/home/robby/sisop/sisop-praktikum-modul-4-2023-bj-a08/soal2/nanaxgerma/src_data";

char *getUserName()
{
    uid_t uid = geteuid();
    struct passwd *pw = getpwuid(uid);
    if (pw)
    {
        return pw->pw_name;
    }
    return "";

}

void logged(char *level, char *cmd, char *desc)
{
    FILE *fp;
   
    fp = fopen("/home/robby/sisop/sisop-praktikum-modul-4-2023-bj-a08/soal2/logmucatatsini.txt", "a+");
    
    if (fp == NULL){
        printf("[Error] : [Gagal dalam membuka file]");
        exit(1);
    }
    char *user = getUserName();
    char *description = malloc(10000 * sizeof(char));
    sprintf(description, "%s-%s", user, desc);

    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    fprintf(fp, "[%s]::%02d/%02d/%04d-%02d:%02d:%02d::[%s]::[%s]\n", level, tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec, cmd, description);
    fclose(fp);
}

static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}


static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}


static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

/**
* Buatlah folder productMagang pada folder /src_data/germa/products/restricted_list/.
* Kemudian, buatlah folder projectMagang pada /src_data/germa/projects/restricted_list/.
* Akan tetapi, hal tersebut akan gagal.
*/ 

static int xmp_mkdir(const char *path, mode_t mode)
{
    int res;
    char fpath[10000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);


    char *desc = malloc(9000);
    if (strstr(fpath, "restricted") != NULL)
    {
        sprintf(desc, "Create directory %s", fpath);
        logged("FAILED", "MKDIR", desc);
        return -1;
    }

    res = mkdir(fpath, mode);

    sprintf(desc, "Create directory %s", fpath);
    logged("SUCCESS", "MKDIR", desc);

    if (res == -1) return -errno;
    return 0;

}


static int xmp_rename(const char *from, const char *to)
{
    int res;
    char ffrom[1000];
    char fto[1000];


    if (strcmp(from, "/") == 0)
    {
        from = dirpath;
        sprintf(ffrom, "%s", from);
    }
    else sprintf(ffrom, "%s%s", dirpath, from);

    if (strcmp(to, "/") == 0)
    {
        to = dirpath;
        sprintf(fto, "%s", to);
    }
    else sprintf(fto, "%s%s", dirpath, to);

    int flag = 0;

    if (strstr(ffrom, "bypass") != NULL || strstr(ffrom, "sihir") != NULL)
    {
        flag =  1;
    }

    if (strstr(ffrom, "restricted") != NULL){ 
        flag = 0;
    }

    if (strstr(fto, "bypass") != NULL || strstr(fto, "sihir") != NULL)
    {
        flag =  1;
    }
    if (strstr(ffrom, "bypass") == NULL && strstr(ffrom, "sihir") == NULL && strstr(ffrom, "restricted") == NULL){
        flag = 1;
    }

    char *desc = malloc(9000);
    if (flag == 0){
        sprintf(desc, "Rename from %s to %s", ffrom, fto);
        logged("FAILED", "RENAME", desc);
        return -1;
    }

    sprintf(desc, "Rename from %s to %s", ffrom, fto);    
    res = rename(ffrom, fto);

    if (res == -1) return -errno;
    logged("SUCCESS", "RENAME", desc);

    return 0;
}

static void xmp_destroy(void *private_data)
{
    char *desc = "Unmount";
    logged("SUCCESS", "UNMOUNT", desc);
}

static int xmp_rmdir(const char *path)
{
    int res;
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);

     int bypass = 0;

    if (strstr(fpath, "sihir") != NULL)
    {
        bypass = 1;
    }
    if (strstr(fpath, "bypass") != NULL)
    {
        bypass = 1;
    }

    char *desc = malloc(9000);
    if (strstr(fpath, "restricted") != NULL || bypass != 1)
    {
        sprintf(desc, "Remove directory %s", fpath);
        logged("FAILED", "RMDIR", desc);
        return -1;
    }

    res = rmdir(fpath);

    sprintf(desc, "Remove directory %s", fpath);
    logged("SUCCESS", "RMDIR", desc);

    if (res == -1) return -errno;
    return 0;
}

static int xmp_unlink(const char *path){

    int res;
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);
    int bypass = 0;

    if (strstr(fpath, "sihir") != NULL)
    {
        bypass = 1;
    }
    if (strstr(fpath, "bypass") != NULL)
    {
        bypass = 1;
    }

    char *desc = malloc(9000);
    if (strstr(fpath, "restricted") != NULL && bypass != 1)
    {
        sprintf(desc, "Remove file %s", fpath);
        logged("FAILED", "UNLINK", desc);
        return -1;
    }

    res = unlink(fpath);

    sprintf(desc, "Remove file %s", fpath);
    logged("SUCCESS", "UNLINK", desc);
    if (res == -1) return -errno;

    return res;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .destroy = xmp_destroy,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
};

int main (int argc, char *argv[]){
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}


// Run
// gcc -Wall `pkg-config fuse --cflags` germa.c -o germa `pkg-config fuse --libs`
// ./germa -f /home/[user]/sisop/modul4/nanaxgerma
